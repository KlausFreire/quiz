<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = 'usuario';
    protected $fillable = [
        'id', 'nome', 'email'
    ];

    public function resolucoes()
    {
        return $this->hasMany(Resolucao::class);
    }
}
