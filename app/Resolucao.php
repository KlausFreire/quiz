<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resolucao extends Model
{
    protected $table = 'resolucao';
    protected $fillable = [
        'id', 'usuario_id', 'questionario_id', 'finalizado',
        'data_inicio', 'tempo', 'total_acertos'
    ];

    public function usuario()
    {
        return $this->belongsTo(Usuario::class);
    }
    public function questionario()
    {
        return $this->belongsTo(Questionario::class);
    }
    public function respostas()
    {
        return $this->hasMany(Resposta::class);
    }
}
