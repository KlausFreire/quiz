<?php

namespace App\Http\Controllers;

use App\Resposta;
use App\Questionario;
use App\Usuario;
use App\Alternativa;
use App\Pergunta;
use App\Resolucao;
use DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

class RespostaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return DB::transaction(function() use($request){
            
            $resolucao = Resolucao::find($request->resolucao_id);
            $tempoLimite = Carbon::createFromFormat('Y-m-d H:i:s',$resolucao->data_inicio)->addMinutes(Questionario::TEMPO_LIMITE);
            if(Carbon::now()->greaterThan($tempoLimite))
            {
                return ['erro' => 'Tempo Esgotado!'];
            }
            
            $pergunta =  Pergunta::find($request->pergunta_id);
            if(!$resolucao || !$pergunta)
            {
                return ['erro' => 'Erro ao salvar resposta, tente novamente'];
            }
            $respostaAnterior = $resolucao->respostas->whereIn('alternativa_id', $pergunta->alternativas->pluck('id'))->first();
            if(isset($respostaAnterior))
            {
                if($respostaAnterior->alternativa_id == $request->alternativa_id)
                {
                    return;
                }
                $respostaAnterior->delete();
            }
            Resposta::create(['resolucao_id' => $resolucao->id, 'alternativa_id' => $request->alternativa_id]);
            if($request->final == 'true')
            {
                $resolucao = Resolucao::find($request->resolucao_id);
                $resolucao->finalizado = true;
                $resolucao->tempo = Carbon::createFromFormat('Y-m-d H:i:s',$resolucao->data_inicio, 'America/Sao_Paulo')->diffInSeconds(Carbon::now('America/Sao_Paulo'));
                $resolucao->total_acertos = $this->getTotalAcertos($resolucao->respostas);
                $resolucao->save();
                return 'finalizado';
            }
        });
    }

    public function getTotalAcertos($respostas)
    {
        $total = 0;
        foreach($respostas as $resposta)
        {
            if($resposta->alternativa->certa)
            {
                $total += 1;
            }
        }
        return $total;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Resposta  $resposta
     * @return \Illuminate\Http\Response
     */
    public function show(Resposta $resposta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Resposta  $resposta
     * @return \Illuminate\Http\Response
     */
    public function edit(Resposta $resposta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Resposta  $resposta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Resposta $resposta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Resposta  $resposta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resposta $resposta)
    {
        //
    }
}
