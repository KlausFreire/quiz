<?php

namespace App\Http\Controllers;

use App\Questionario;
use App\Usuario;
use App\Resolucao;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;

class QuestionarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hoje = Carbon::now('America/Sao_Paulo');
        // dd($hoje->format('d/m/Y'));
        $questionario = Questionario::where('dia', $hoje->format('Y-m-d'))->first();
        if(!$questionario)
        {
            $proximoQuestionario = Questionario::where('dia', '>=', $hoje->format('Y-m-d'))->oldest('dia')->first();
            $dia = null;
            if ($proximoQuestionario) {
                $dia = Carbon::createFromFormat('Y-m-d',$proximoQuestionario->dia, 'America/Sao_Paulo');
                $dias = $hoje->diffInDays($dia);
            }
            
            return view('welcome', compact('dia', 'dias'));
        }
        return view('entrar');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validacao($request);
        $inputs = $request->except(['_token']);
        $usuario = $this->createUsuario($inputs);
        $questionario = Questionario::where('dia', date('Y-m-d'))->first();
        if(!$questionario)
        {
            return view('quiz_indisponivel');
        }
        $resolucao = Resolucao::where('usuario_id', $usuario->id) 
                                ->where('questionario_id', $questionario->id)->first();
        if($resolucao)
        {
            if($resolucao->finalizado)
            {
                return redirect('concluido/' . $resolucao->id);
            }
        }else
        {
            $resolucao = DB::transaction(function() use($questionario, $usuario){
                return Resolucao::create(['usuario_id' => $usuario->id,
                                    'questionario_id' => $questionario->id,
                                    'data_inicio' => date('Y-m-d H:i:s')]);
            });
            //por algum motivo a data_inicio vem nula quando é criada, entao faz-se uma nova busca
            $resolucao = Resolucao::find($resolucao->id);
        }
        $tempoLimite = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$resolucao->data_inicio)
                                        ->addMinutes(Questionario::TEMPO_LIMITE)
                                        ->toIso8601String();
        return view('quiz_resolucao', compact('resolucao', 'tempoLimite'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Questionario  $questionario
     * @return \Illuminate\Http\Response
     */
    public function show(Questionario $questionario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Questionario  $questionario
     * @return \Illuminate\Http\Response
     */
    public function edit(Questionario $questionario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Questionario  $questionario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Questionario $questionario)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Questionario  $questionario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Questionario $questionario)
    {
        //
    }

    public function validacao(Request $request)
    {
        $this->validate($request,
            ['nome' => 'required',
            'email' => 'required|email'],
            ['nome.required' => 'Nome é obrigatório.',
            'email.required' => 'E-mail é obrigatório.',
            'email.email' => 'Deve ser informado um endereço de e-mail válido.']);
    }

    public function createUsuario(array $inputs)
    {
        return DB::transaction(function() use($inputs){
            $usuario = Usuario::where('email', $inputs['email'])->first();
            if(!$usuario)
            {
                $usuario = Usuario::create($inputs);
            }else
            {
                $usuario->nome = $inputs['nome'];
                $usuario->save();
            }
            return $usuario;
        });
    }
}
