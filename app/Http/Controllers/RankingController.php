<?php

namespace App\Http\Controllers;

use App\Questionario;
use App\Resolucao;
use Illuminate\Http\Request;

class RankingController extends Controller
{
    public function rankingDia()
    {
        $questionario = Questionario::where('dia', date('Y-m-d'))->first();
        $resolucoes = [];
        
        if ($questionario) {
            $resolucoes = Resolucao::where('questionario_id', $questionario->id)
                        ->where('finalizado', 1)->orderBy('total_acertos', 'desc')
                        ->orderBy('tempo', 'asc')->limit(10)->get();
        }

        return view('ranking_diario', compact('resolucoes'));
    }
    public function rankingGeral()
    {
        $questionarios = Questionario::orderBy('dia')->get();
        $ranking = [];
        foreach($questionarios as $questionario) {
            $resolucoes = Resolucao::where('questionario_id', $questionario->id)
                        ->where('finalizado', 1)->orderBy('total_acertos', 'desc')
                        ->orderBy('tempo', 'asc')->limit(5)->get();

            $ranking[$questionario->dia] = $resolucoes;
        }

        return view('ranking_geral', compact('ranking'));
    }
}
