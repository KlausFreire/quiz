<?php

namespace App\Http\Controllers;

use App\Resolucao;
use Illuminate\Http\Request;

class ResolucaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Resolucao  $resolucao
     * @return \Illuminate\Http\Response
     */
    public function show(Resolucao $resolucao)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Resolucao  $resolucao
     * @return \Illuminate\Http\Response
     */
    public function edit(Resolucao $resolucao)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Resolucao  $resolucao
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Resolucao $resolucao)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Resolucao  $resolucao
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resolucao $resolucao)
    {
        //
    }

    public function concluido($id)
    {
        $resolucao = Resolucao::find($id);
        if(!$resolucao)
        {
            return redirect('/');
        }
        $resolucoes = Resolucao::where('finalizado', 1)->orderBy('total_acertos', 'desc')
                        ->whereDate('data_inicio', date('Y-m-d'))
                        ->orderBy('tempo', 'asc')->get();
        $posicao = 1;
        foreach($resolucoes as $r) {

            if ($r->id == $id) {
                break;
            }
            ++$posicao;
        }
        
        return view('quiz_concluido', compact('resolucao', 'posicao'));
    }
}
