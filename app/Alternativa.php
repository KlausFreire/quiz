<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alternativa extends Model
{
    protected $table = 'alternativa';
    protected $fillable = [
        'id', 'texto', 'pergunta_id', 'certa'
    ];

    public function pergunta()
    {
        return $this->belongsTo(Pergunta::class);
    }
    public function respostas()
    {
        return $this->hasMany(Resposta::class);
    }
}
