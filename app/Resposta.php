<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resposta extends Model
{
    protected $table = 'resposta';
    protected $fillable = [
        'id', 'resolucao_id', 'alternativa_id'
    ];

    public function resolucao()
    {
        return $this->belongsTo(Resolucao::class);
    }
    public function alternativa()
    {
        return $this->belongsTo(Alternativa::class);
    }
}
