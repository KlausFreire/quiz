<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pergunta extends Model
{
    protected $table = 'pergunta';
    protected $fillable = [
        'id', 'texto', 'questionario_id'
    ];

    public function questionario()
    {
        return $this->belongsTo(Questionario::class);
    }
    public function alternativas()
    {
        return $this->hasMany(Alternativa::class);
    }
}
