<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionario extends Model
{
    const TEMPO_LIMITE = 2;
    protected $table = 'questionario';
    protected $fillable = [
        'id', 'dia', 'tempo_limite'
    ];

    public function perguntas()
    {
        return $this->hasMany(Pergunta::class);
    }
    public function resolucoes()
    {
        return $this->hasMany(Resolucao::class);
    }
}
