@extends('layout')
@section('content')
<div class="jumbotron">
    <div class="container">
        <img src="/images/logo-consorcio.png" class="mx-auto d-block logo-small" alt="consorcio JCJ">
        <div class="col-sm-7 offset-md-2 text-center">
            @if($resolucao->total_acertos >= 10)
            <h2 class="big-font h1">Parabéns {{$resolucao->usuario->nome}}!!!</h2>
            <p class="big-font h4">Apresente esta tela no espaço Consórcio PCJ para pegar sua surpresa!</p>
             
            <p class="">
                {{--
                Ranking: <span class="num-rank destaque">{{$posicao}}º<span data-feather="award"></span></span><br/>
                --}}
                Acertos: <strong class="destaque">{{$resolucao->total_acertos}}</strong><br/>
                Tempo: <strong class="destaque">{{gmdate("H:i:s", $resolucao->tempo)}}</strong><br><br>
            </p>
            @else
            <h2 class="big-font h1">Obrigado pela participação.</h2>
            <p class="big-font h4">Melhore seus conhecimentos sobre a água e tente novamente nos próximos dias do Fórum!</p>
            <p class="">
                {{--
                Ranking: <span class="num-rank destaque">{{$posicao}}º<span data-feather="award"></span></span><br/>
                --}}
                Acertos: <strong class="destaque">{{$resolucao->total_acertos}}</strong><br/>
                Tempo: <strong class="destaque">{{gmdate("H:i:s", $resolucao->tempo)}}</strong><br><br>
            </p>
            @endif
            {{-- 
            <a class="btn btn-success btn-wide" href="{{url('/ranking_dia')}}">Ir para o Ranking</a>
            --}}
        </div>
    </div>
</div>
@endsection