@extends('layout')
@section('content')
<div class="jumbotron jumbotron-fluid slide-1 " style="display: none">
  <div class="container col-sm-12 row justify-content-center">
      <div class="col-md-3 left-block">
          <img src="/images/logo-consorcio.png" class="mx-auto d-block logo-vert">
      </div>
    <div>
        <h2 class="text-center">Participe do desafio Tudo sobre água em 2 minutos!</h2>
        <img src="/images/qrcode.svg" class="mx-auto d-block qrcode">
        <p class="lead text-center h3">ou acesse 
        <strong class="destaque"><a href="https://goo.gl/rbf7XX">https://goo.gl/rbf7XX</a></strong></p>
    </div>
  </div>
</div>
<div class="jumbotron jumbotron-fluid slide-0">
  <div class="container col-sm-12 row">
    <div class="col-md-3 left-block">
      <img src="/images/logo-consorcio.png" class="mx-auto d-block logo-vert">
      <br>
      @if(!empty($resolucoes))
      <br>
      <h2 class="text-center big-font">
          <strong class="destaque">Ranking</strong>
      </h2>
      @endif
    </div>
    <div class="col-sm-8">
      <h5 class="text-center big-font">
        @php
            $resultadoFinal = (boolean) (date('His') >= '190000' && date('His') <= '235959');
        @endphp
        @if($resultadoFinal)
          <strong class="destaque">Confira quem foram os ganhadores de hoje do Desafio do Consórcio PCJ</strong>
        @else
          <strong class="destaque">Confira quem são os melhores colocados até o momento</strong>
        @endif
      </h5>
        @forelse($resolucoes as $resolucao)
        @if($resultadoFinal && $loop->index == 5)
        @break
        @endif
        <div class="lead listagem row justify-content-between col-sm-12">
            <p class="rank destaque row col-xs-2">
              <span class="num-rank">{{$loop->index + 1}}°<span data-feather="award"></span></span>
            </p>
            <span class="col-sm-7">{{strtoupper($resolucao->usuario->nome)}}</span> 
            <span class="aux acertos col-sm-2">Acertos: <strong class="destaque">{{$resolucao->total_acertos}}</strong></span>
            <span class="aux destaque mdfont col-sm-2 d-flex justify-content-end"><strong> {{gmdate("H:i:s", $resolucao->tempo)}}</strong></span>
        </div>
        @empty
        <div class="lead text-center listagem">
            <h2 class="text-center">
            Seja o 1º a participar do nosso Quiz.</h2>
            <img src="/images/qrcode.svg" class="mx-auto d-block">
            <p class="lead text-center h2">ou acessando diretamente a página 
            <strong class="destaque"><a href="https://goo.gl/rbf7XX">https://goo.gl/rbf7XX</a></strong></p>
        </div>
        @endforelse
    </div>
  </div>
</div>
@endsection
@section('script')
<script>
var slide = 1;
/*
setInterval(function(){  
    if (slide == 1) {
        $('.slide-0').fadeOut(1000, function(){
            $('.slide-1').fadeIn(1000);
        });
        ++slide;
    } else if (slide == 2) {
        $('.slide-1').fadeOut(1000, function(){
            location.reload(true);
            //$('.slide-0').fadeIn(1000);
        });
        ++slide;
    } else {
        location.reload(true);
    }
}, 15000);
*/
</script>
@endsection