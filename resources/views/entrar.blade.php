@extends('layout')
@section('content')
<div class="jumbotron content">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-1"></div>
      <div class="col-sm-12 col-md-10">
        <img src="/images/logo-consorcio.png" class="mx-auto d-block logo-small" alt="consorcio JCJ">
        <br/>
          @php
              $resultadoFinal = (boolean) (date('His') >= '190000' && date('His') <= '235959');
              $resultadoFinal = false;
          @endphp
          @if($resultadoFinal)
            <h1 class="destaque text-center">O QUIZ DE HOJE FOI ENCERRADO.</h1>  
            <h1 class="destaque text-center">Disponível até às 19h</h1>  
          @else
          <p class="lead text-center important-info">
            PARTICIPE DO DESAFIO TUDO SOBRE ÁGUA EM 2 MINUTOS! <br>
            São apenas 10 perguntas para responder em até dois minutos e concorrer a surpresas exclusivas do Consórcio PCJ! <br>
            Deixe aqui seu nome e e-mail para começar:
          </p>
          @endif
          <div class="container">
            <div class="row">
              <div class="col-md-3 col-sm-1"></div>
              <div class="col-md-6 col-sm-10">
                <form class="" action="{{url('/quiz')}}" method="POST" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  @if(!$resultadoFinal)
                  <div class="form-group">
                      <!-- <label for="nome">Seu nome completo</label> -->
                      <input class="form-control input-style" type="text" name="nome" required="required" placeholder="Seu nome completo">
                  </div>
                  <div class="form-group">
                      <!-- <label for="nome">Seu melhor e-mail</label>                         -->
                      <input class="form-control input-style" type="email" name="email" required="required" placeholder="Seu melhor email">
                  </div>
                  <div class="col-sm-12 row justify-content-center">
                    <input class="form-group btn btn-success btn-lg btn-iniciar" type="submit" value="Quero iniciar o quiz agora!">                    
                  </div>
                  <div class="col-sm-12 row justify-content-center">
                    <small>ou</small>                    
                  </div>
                  @endif
                  <div class="col-sm-12 row justify-content-center">
                    <a class="form-group btn text-light" href="/resultado">Acompanhar Ranking Geral</a>                    
                  </div>
                </form>
              </div>
              <div class="col-md-3 col-sm-1"></div>
            </div>
          </div>
        <div {{$errors->any()? '':'style=display:none'}}>
          @foreach($errors->all() as $error)
          <ul style="color: red;">
              <li >{{$error}}</li>
          </ul>
          @endforeach
        </div>
      </div>
      <div class="col-sm-12 col-md-1"></div>
    </div>
  </div>
</div>
@endsection