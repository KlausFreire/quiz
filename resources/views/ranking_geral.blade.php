@extends('layout')
@section('content')
<div class="jumbotron jumbotron-fluid">
  <div class="container col-sm-12 row">
    <div class="col-sm-12">
        @forelse($ranking as $dia => $participantes)
        <h2 class="text-center big-font">
          <strong class="destaque">Ranking do dia {{date('d/m/Y', strtotime($dia))}}</strong>
        </h2>
        @forelse($participantes as $resolucao)
        <div class="lead listagem row justify-content-between col-sm-12">
            <p class="rank destaque row col-xs-2">
              <span class="num-rank">{{$loop->index + 1}}°<span data-feather="award"></span></span>
            </p>
            
            <span class="col-sm-7">
                {{strtoupper($resolucao->usuario->nome)}}
            </span> 
            <span class="aux acertos col-sm-2">Acertos: <strong class="destaque">{{$resolucao->total_acertos}}</strong></span>
            <span class="aux destaque mdfont col-sm-2 d-flex justify-content-end"><strong> {{gmdate("H:i:s", $resolucao->tempo)}}</strong></span>
        </div>
        @empty
        <div class="lead text-center listagem">
            <h2 class="text-center">Ainda não há resultados.</h2>
        </div>
        @endforelse
        @empty
        <div class="lead text-center listagem">
            <h2 class="text-center">Ainda não há resultados.</h2>
        </div>
        @endforelse
    </div>
  </div>
</div>
@endsection
