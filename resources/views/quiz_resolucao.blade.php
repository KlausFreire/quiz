@extends('layout')
@section('content')
@php $qtdPerguntas = count($resolucao->questionario->perguntas); @endphp
<div class="jumbotron">
    <div class="container">
        <div class="col-sm-8 offset-sm-2">
            <div id="clockbox" class="flashBorder row justify-content-center">
                <span id="timer" class="timer text-secondary" data-feather="clock"></span>
                <span id="clock" class="text-center text-secondary"></span>
            </div>
            @foreach($resolucao->questionario->perguntas as $index=>$pergunta)
            <div class="questoes" id="pergunta_{{$pergunta->id}}" {{$index > 0 ? 'style=display:none': ''}}>
                <div class="text-center h4 destaque"><small>Questão <b>{{$loop->index + 1}}</b> de <b>10</b></small></div>
                <p class="pergunta">
                    {{$pergunta->texto}}
                </p> 
                @foreach($pergunta->alternativas as $alternativa)
                <div class="custom-control custom-radio" style="margin: 5px;">
                    <input type="radio" class="custom-control-input alternativa_{{$pergunta->id}}"
                        id="alternativa_{{$alternativa->id}}" name="pergunta_{{$pergunta->id}}" value="{{$alternativa->id}}">
                    <label class="custom-control-label" for="alternativa_{{$alternativa->id}}">{{$alternativa->texto}}</label>
                </div>
                @endforeach
            </div>
            @endforeach
        </div>
        <br/>
        <div class="col-sm-12 row justify-content-center">
            <button class="btn btn-secondary btn-wide btn-lg" id="voltar" onclick="previous()" style="display:none">Anterior</button>                    
            <button class="btn btn-success btn-wide btn-lg" id="proxima" onclick="next()">Próxima</button>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js"></script>
<script>
    $(document).ready(function() {
        $(".clockbox")

    });
</script>
<script>
    var visible = {{$resolucao->questionario->perguntas->first()->id}};
    var perguntas = {{$resolucao->questionario->perguntas->pluck('id')}}
    
    
    
    function next()
    {
        var currentIndex = perguntas.indexOf(visible)
        if((currentIndex + 1) == perguntas.length)
        {
            //TODO encerrar questionario
            sendAnswer(true);
        }else
        {
            if(sendAnswer())
            {
                var nextQuestion = perguntas[currentIndex +1];
                $('#pergunta_' + visible).prop('style', 'display:none');
                $('#pergunta_' + nextQuestion).prop('style', 'display:block');
                $('#voltar').prop('style', 'display:inline-block');            
                visible = nextQuestion;
                if((currentIndex + 2) == perguntas.length)
                {
                    $('#proxima').text('Enviar');
                }
            }
        }
    }
    function previous()
    {
        var currentIndex = perguntas.indexOf(visible)
        if(currentIndex > 0)
        {
            var previousQuestion = perguntas[currentIndex -1];
            $('#pergunta_' + visible).prop('style', 'display:none');
            $('#pergunta_' + previousQuestion).prop('style', 'display:block');
            $('#proxima').text('Próxima');            
            visible = previousQuestion;
            if((currentIndex - 1) <= 0)
            {
                $('#voltar').prop('style', 'display:none'); 
            }
        }
    }
    function sendAnswer(final = false){
        var alternativa = $('input[name=pergunta_' + visible +']:checked').val();
        if(!alternativa)
        {
            window.alert('Selecione uma alternativa!');
            return false;
        }
        $.ajax({
            url:'{{url('/resposta')}}',
            method: "POST",
            data: {'resolucao_id': {{$resolucao->id}},
                    'alternativa_id': alternativa,
                    'pergunta_id': visible,
                    'final': final,
                    '_token': '{{ csrf_token() }}'},
            success:function(data){
                if(data['erro'])
                {
                    window.alert(data['erro']);
                    if(data['erro'] == 'Tempo Esgotado!')
                    {
                        window.location.href = '{{url('/')}}';
                    }
                }
                if(data == 'finalizado')
                {
                    window.location.href = '{{url('/concluido/'. $resolucao->id )}}';
                }
            },error:function(error){
                console.log(error);
                window.alert('Erro ao registrar resposta! Tente novamente.');
            }
        });
        return true;
    }


    // Set the date we're counting down to
    var stringCountDownDate = "{{$tempoLimite}}";
    var countDownDate = new Date(stringCountDownDate).getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get todays date and time
        var now = new Date().getTime();
        
        // Find the distance between now an the count down date
        var distance = countDownDate - now;
        
        // Time calculations for days, hours, minutes and seconds
        // var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Output the result in an element with id="clock"
        document.getElementById("clock").innerHTML = hours + "h "
        + minutes + "m " + seconds + "s ";
        
        // If the count down is over, write some text 
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("clock").innerHTML = "Tempo esgotado";
            document.getElementById("timer").style.display = "none";
            document.getElementById("clockbox").classList.remove("flashBorder");
        }
    }, 1000);
</script>
@endsection