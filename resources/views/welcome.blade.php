@extends('layout')
@section('content')
<div class="jumbotron jumbotron-fluid slide-0">
  <div class="container">
    <img src="/images/logo-consorcio.jpg" class="mx-auto d-block img-head">
    <br>
    @if($dia)
    <h2 class="text-center margin-vert">
        A partir de <strong class="destaque">{{$dias > 1 ? $dia->format('d/m') : 'amanhã'}}</strong>, 
        participe do quiz e concorra a diversos brindes e materiais educativos</h2>
        <br>
        <img src="/images/qrcode.svg" class="mx-auto d-block">
        <p class="lead text-center">ou acessando diretamente a página 
        <strong class="destaque"><a href="https://goo.gl/rbf7XX">https://goo.gl/rbf7XX</a></strong></p>
    @else
    <br/>
    <h2 class="text-center destaque">ENCERRADO</h2>
    <p class="lead text-center">
          Infelizmente nosso quiz se encerrou. <br/>Agradecemos a todos os participantes 
          e até uma próxima oportunidade!
      </p>
    @endif
  </div>
</div>
@endsection

