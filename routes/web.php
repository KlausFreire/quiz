<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('quiz');
});

Route::resource('quiz', 'QuestionarioController');
Route::resource('resposta', 'RespostaController');
Route::get('concluido/{id}', 'ResolucaoController@concluido');
Route::get('ranking_dia', 'RankingController@rankingDia');
Route::get('resultado', 'RankingController@rankingGeral');