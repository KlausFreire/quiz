<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(QuestionarioSeeder::class);
        $this->call(PerguntaSeeder::class);
        $this->call(AlternativaSeeder::class);
        
    }
}
