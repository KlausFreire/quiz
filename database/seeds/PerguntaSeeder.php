<?php

use Illuminate\Database\Seeder;
use App\Questionario;
use App\Pergunta;

class PerguntaSeeder extends Seeder
{

    public function perguntas()
    {
        return [
            ['questionario_id' => 1, 'texto' => 'Uma torneira mal fechada (água em filete) pode causar a perda de:'],
            ['questionario_id' => 1, 'texto' => 'Uma torneira pingando gasta cerca de: '],
            ['questionario_id' => 1, 'texto' => 'Lavar a calçada com mangueira desperdiça, em média:'],
            ['questionario_id' => 1, 'texto' => 'O Sistema Cantareira é:'],
            ['questionario_id' => 1, 'texto' => 'Lavar o carro com mangueira desperdiça, em média:'],
            ['questionario_id' => 1, 'texto' => 'Escovar os dentes com a torneira aberta desperdiça, em média:'],
            ['questionario_id' => 1, 'texto' => 'Uma válvula de descarga comum gasta cerca de:'],
            ['questionario_id' => 1, 'texto' => 'Uma boa gestão ambiental necessita da participação:'],
            ['questionario_id' => 1, 'texto' => '15 minutos de banho proporciona um gasto médio de: '],
            ['questionario_id' => 1, 'texto' => 'O Planeta Terra é composto de:'],
            ['questionario_id' => 2, 'texto' => 'O que significa a sigla “PCJ”?'],
            ['questionario_id' => 2, 'texto' => 'O consumo médio de água por pessoa recomendado pela ONU é: '],
            ['questionario_id' => 2, 'texto' => 'Existem mais _______ do que banheiros ligados à rede de esgotos:'],
            ['questionario_id' => 2, 'texto' => 'Jogar comida e óleo na pia e depositar lixo no vaso sanitário causam:'],
            ['questionario_id' => 2, 'texto' => 'Consórcio PCJ é:'],
            ['questionario_id' => 2, 'texto' => 'O Consórcio possui como associados:'],
            ['questionario_id' => 2, 'texto' => 'Qual o outro nome para a chuva, que é o retorno da água à terra?'],
            ['questionario_id' => 2, 'texto' => 'O que mantém o movimento da água dos rios e córregos na direção dos mares e oceanos?'],
            ['questionario_id' => 2, 'texto' => 'Qual atividade que mais consome água segundo a Organização das Nações Unidas para a Alimentação e Agricultura (FAO)?'],
            ['questionario_id' => 2, 'texto' => 'O ciclo chamado Ciclo Hidrológico é o ciclo do (a):'],
            ['questionario_id' => 3, 'texto' => 'A ONU diz que, caso não mudemos nossos hábitos de consumo de água, enfrentaremos um déficit de 40% no abastecimento em determinada data. Em qual ano seria isso?'],
            ['questionario_id' => 3, 'texto' => 'As geleiras concentram uma boa parte da água doce do planeta. Um sinônimo para geleira é:'],
            ['questionario_id' => 3, 'texto' => 'As matas ciliares são importantes para:'],
            ['questionario_id' => 3, 'texto' => 'A ligação das tubulações de água pluvial na rede de esgoto:'],
            ['questionario_id' => 3, 'texto' => 'Quais as características que a água de consumo deve ter?'],
            ['questionario_id' => 3, 'texto' => 'Quais são as principais doenças veiculadas pela água?'],
            ['questionario_id' => 3, 'texto' => 'Raízes das árvores e o húmus conservam:'],
            ['questionario_id' => 3, 'texto' => 'O que devemos fazer com o lixo doméstico?'],
            ['questionario_id' => 3, 'texto' => 'É possível tomar um banho saudável em apenas:'],
            ['questionario_id' => 3, 'texto' => 'O índice de perdas de água na distribuição (ou seja, de água tratada que não chega às residências porque é perdida ou furtada) no Brasil é de aproximadamente:'],
            ['questionario_id' => 4, 'texto' => 'Qual é o estado que consome mais água no Brasil, considerando o consumo médio por pessoa?'],
            ['questionario_id' => 4, 'texto' => 'Qual é o maior rio presente na superfície terrestre?'],
            ['questionario_id' => 4, 'texto' => 'Para que seja possível ocorrer a formação de um aquífero, a precipitação terá de ser tal que ultrapasse o nível de ____ das formações geológicas.'],
            ['questionario_id' => 4, 'texto' => 'O que é drenar?'],
            ['questionario_id' => 4, 'texto' => 'O que é mata ciliar?'],
            ['questionario_id' => 4, 'texto' => 'A função das raízes de árvores ciliares para rios e nascentes é evitar:'],
            ['questionario_id' => 4, 'texto' => 'Qual é o nome dos tanques utilizados no processo de tratamento do esgoto?'],
            ['questionario_id' => 4, 'texto' => 'Como são chamadas as fontes de água superficiais ou subterrâneas, que podem ser utilizadas para o abastecimento público?'],
            ['questionario_id' => 4, 'texto' => 'Como é chamado o estudo da distribuição, movimento e qualidade da água?'],
            ['questionario_id' => 4, 'texto' => 'Uma das formas de colaborar com a preservação do meio ambiente é reduzir a produção de resíduos como:'],
            ['questionario_id' => 5, 'texto' => 'Quais são os serviços ambientais prestados somente pela água, sem interferência do ser humano?'],
            ['questionario_id' => 5, 'texto' => 'Cobrança pelo uso de água é um importante instrumento para a:'],
            ['questionario_id' => 5, 'texto' => 'Como podemos preservar árvores e florestas?'],
            ['questionario_id' => 5, 'texto' => 'Quais dos elementos abaixo não é utilizado como fonte de energia?'],
            ['questionario_id' => 5, 'texto' => 'Qual o ponto de ebulição da água?'],
            ['questionario_id' => 5, 'texto' => 'Quanto tempo uma pessoa consegue sobreviver sem ingerir água?'],
            ['questionario_id' => 5, 'texto' => 'Em porcentagem, quanto de água existe no nosso corpo?'],
            ['questionario_id' => 5, 'texto' => 'Qual a fórmula da água?'],
            ['questionario_id' => 5, 'texto' => 'Em porcentagem, o cérebro é constituído por qual quantidade de água?'],
            ['questionario_id' => 5, 'texto' => 'Qual afirmação abaixo está incorreta em relação às matas ciliares:'],
            
        ];
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $questionarios = Questionario::all();
        // Seed estático
        // foreach($questionarios as $questionario)
        // {
        //     if(count($questionario->perguntas) == 0)
        //     {
        //         for($i = 0; $i < 5; $i++)
        //         {
        //             Pergunta::create(['questionario_id' => $questionario->id,
        //                             'texto' => 'questionário '. $questionario->id . ' pergunta ' . ($i + 1)]);
        //         }
        //     }
        // }
        foreach($this->perguntas() as $pergunta)
        {
            Pergunta::create($pergunta);
        }
    }
}
