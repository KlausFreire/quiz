<?php

use Illuminate\Database\Seeder;
use App\Questionario;

class QuestionarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dia = Carbon\Carbon::createFromFormat('Y-m-d', '2018-03-18', 'America/Sao_Paulo');;
        for($i = 0; $i < 5; $i++)
        {
            Questionario::create(['dia' => $dia->addDay(),
                                'tempo_limite' => 1]);
        }
    }
}
