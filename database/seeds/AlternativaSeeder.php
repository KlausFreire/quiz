<?php

use Illuminate\Database\Seeder;
use App\Pergunta;
use App\Alternativa;

class AlternativaSeeder extends Seeder
{

    public function alternativas()
    {
        return [
            ['pergunta_id' => 1, 'certa' => 0, 'texto' => '1 a 7 litros de água/dia'],
            ['pergunta_id' => 1, 'certa' => 0, 'texto' => '18 a 72 litros de água/dia'],
            ['pergunta_id' => 1, 'certa' => 1, 'texto' => '180 a 720 litros de água/dia'],
            ['pergunta_id' => 1, 'certa' => 0, 'texto' => '1.000 a 2.000 litros de água/dia'],
            ['pergunta_id' => 2, 'certa' => 0, 'texto' => '3 litros de água/dia'],
            ['pergunta_id' => 2, 'certa' => 1, 'texto' => '45 litros de água/dia'],
            ['pergunta_id' => 2, 'certa' => 0, 'texto' => '300 litros de água/dia'],
            ['pergunta_id' => 2, 'certa' => 0, 'texto' => '1.000 litros de água/dia'],
            ['pergunta_id' => 3, 'certa' => 0, 'texto' => '2 litros de água'],
            ['pergunta_id' => 3, 'certa' => 0, 'texto' => '40 litros de água'],
            ['pergunta_id' => 3, 'certa' => 1, 'texto' => '270 litros de água'],
            ['pergunta_id' => 3, 'certa' => 0, 'texto' => '900 litros de água'],
            ['pergunta_id' => 4, 'certa' => 0, 'texto' => 'um conjunto de reservatórios localizados na Bacia do Alto Tietê'],
            ['pergunta_id' => 4, 'certa' => 0, 'texto' => 'um conjunto de reservatórios localizados na Bacia do Rio Piracicaba'],
            ['pergunta_id' => 4, 'certa' => 0, 'texto' => 'um conjunto de reservatórios cujas águas são provenientes da Serra da Cantareira'],
            ['pergunta_id' => 4, 'certa' => 1, 'texto' => 'a maior transposição de água entre bacias hidrográficas do mundo'],
            ['pergunta_id' => 5, 'certa' => 0, 'texto' => '9 litros de água'],
            ['pergunta_id' => 5, 'certa' => 0, 'texto' => '80 litros de água'],
            ['pergunta_id' => 5, 'certa' => 1, 'texto' => '380 litros de água'],
            ['pergunta_id' => 5, 'certa' => 0, 'texto' => '790 litros de água'],
            ['pergunta_id' => 6, 'certa' => 1, 'texto' => '18 litros de água'],
            ['pergunta_id' => 6, 'certa' => 0, 'texto' => '60 litros de água'],
            ['pergunta_id' => 6, 'certa' => 0, 'texto' => '120 litros de água'],
            ['pergunta_id' => 6, 'certa' => 0, 'texto' => '300 litros de água'],
            ['pergunta_id' => 7, 'certa' => 0, 'texto' => '3 a 8 litros de água'],
            ['pergunta_id' => 7, 'certa' => 0, 'texto' => '4 a 6 litros de água'],
            ['pergunta_id' => 7, 'certa' => 0, 'texto' => '6 a 10 litros de água'],
            ['pergunta_id' => 7, 'certa' => 1, 'texto' => '10 a 12 litros de água'],
            ['pergunta_id' => 8, 'certa' => 1, 'texto' => 'de todos, para maior eficiência e maiores resultados'],
            ['pergunta_id' => 8, 'certa' => 0, 'texto' => 'do município, para maior eficiência e maiores resultados'],
            ['pergunta_id' => 8, 'certa' => 0, 'texto' => 'dos estudantes, para maior eficiência'],
            ['pergunta_id' => 8, 'certa' => 0, 'texto' => 'dos governantes, para maior eficiência'],
            ['pergunta_id' => 9, 'certa' => 0, 'texto' => '30 litros de água'],
            ['pergunta_id' => 9, 'certa' => 0, 'texto' => '60 litros de água'],
            ['pergunta_id' => 9, 'certa' => 1, 'texto' => '120 litros de água'],
            ['pergunta_id' => 9, 'certa' => 0, 'texto' => '240 litros de água'],
            ['pergunta_id' => 10, 'certa' => 0, 'texto' => '50% de água salgada, 27,8% de água doce nas geleiras e apenas 22,2% de água doce em rios/lagos'],
            ['pergunta_id' => 10, 'certa' => 0, 'texto' => '67% de água salgada, 18,8% de água doce nas geleiras e apenas 14,2 % de água doce em rios/lagos'],
            ['pergunta_id' => 10, 'certa' => 0, 'texto' => '72% de água salgada, 23,8% de água doce nas geleiras e apenas 4,2% de água doce em rios/lagos'],
            ['pergunta_id' => 10, 'certa' => 1, 'texto' => '97% de água salgada, 2,3% de água doce nas geleiras e apenas 0,7% de água doce em rios/lagos'],

            ['pergunta_id' => 11, 'certa' => 0, 'texto' => 'Piracicaba, Campinas e Jundiaí'],
            ['pergunta_id' => 11, 'certa' => 1, 'texto' => 'Piracicaba, Capivari e Jundiaí'],
            ['pergunta_id' => 11, 'certa' => 0, 'texto' => 'Piracicaba, Capivari e Jacareí'],
            ['pergunta_id' => 11, 'certa' => 0, 'texto' => 'nenhuma das alternativas anteriores'],
            ['pergunta_id' => 12, 'certa' => 0, 'texto' => '250 litros/dia'],
            ['pergunta_id' => 12, 'certa' => 0, 'texto' => '210 litros/dia'],
            ['pergunta_id' => 12, 'certa' => 0, 'texto' => '150 litros/dia'],
            ['pergunta_id' => 12, 'certa' => 1, 'texto' => '110 litros/dia'],
            ['pergunta_id' => 13, 'certa' => 0, 'texto' => 'veículos no mundo'],
            ['pergunta_id' => 13, 'certa' => 0, 'texto' => 'lixões no mundo'],
            ['pergunta_id' => 13, 'certa' => 0, 'texto' => 'celulares no mundo'],
            ['pergunta_id' => 13, 'certa' => 1, 'texto' => 'todas as alternativas estão corretas'],
            ['pergunta_id' => 14, 'certa' => 0, 'texto' => 'benefícios ao meio ambiente, pois o volume de resíduos depositados nos aterros sanitários é menor'],
            ['pergunta_id' => 14, 'certa' => 0, 'texto' => 'maior número de matéria orgânica, considerando eficiência no tratamento de esgoto'],
            ['pergunta_id' => 14, 'certa' => 1, 'texto' => 'entupimento e interferências no tratamento do esgoto'],
            ['pergunta_id' => 14, 'certa' => 0, 'texto' => 'aumento da tarifa em relação ao lançamento de esgoto em sua conta de água'],
            ['pergunta_id' => 15, 'certa' => 0, 'texto' => 'Uma atividade para compra de produtos e serviços.'],
            ['pergunta_id' => 15, 'certa' => 0, 'texto' => 'O nome de uma bacia hidrográfica'],
            ['pergunta_id' => 15, 'certa' => 0, 'texto' => 'Projeto de preservação ambiental'],
            ['pergunta_id' => 15, 'certa' => 1, 'texto' => 'Uma associação de usuários de água com foco na preservação da água'],
            ['pergunta_id' => 16, 'certa' => 0, 'texto' => 'somente empresas'],
            ['pergunta_id' => 16, 'certa' => 0, 'texto' => 'somente prefeituras'],
            ['pergunta_id' => 16, 'certa' => 1, 'texto' => 'empresas e prefeituras'],
            ['pergunta_id' => 16, 'certa' => 0, 'texto' => 'empresas, prefeituras e ONGs'],
            ['pergunta_id' => 17, 'certa' => 0, 'texto' => 'granizo'],
            ['pergunta_id' => 17, 'certa' => 0, 'texto' => 'transpiração'],
            ['pergunta_id' => 17, 'certa' => 1, 'texto' => 'precipitação'],
            ['pergunta_id' => 17, 'certa' => 0, 'texto' => 'condensação.'],
            ['pergunta_id' => 18, 'certa' => 0, 'texto' => 'a lei da impenetrabilidade dos corpos'],
            ['pergunta_id' => 18, 'certa' => 1, 'texto' => 'a força da gravidade'],
            ['pergunta_id' => 18, 'certa' => 0, 'texto' => 'a condensação da água'],
            ['pergunta_id' => 18, 'certa' => 0, 'texto' => 'a força das chuvas'],
            ['pergunta_id' => 19, 'certa' => 1, 'texto' => 'a atividade agropecuária, responsável por 70% do uso da água'],
            ['pergunta_id' => 19, 'certa' => 0, 'texto' => 'a indústria, responsável por 66% do uso de água no planeta'],
            ['pergunta_id' => 19, 'certa' => 0, 'texto' => 'o consumo residencial, que responde por 41% do uso de água na Terra'],
            ['pergunta_id' => 19, 'certa' => 0, 'texto' => 'o setor pesqueiro, que gasta 26% da água do planeta'],
            ['pergunta_id' => 20, 'certa' => 0, 'texto' => 'ar'],
            ['pergunta_id' => 20, 'certa' => 0, 'texto' => 'vento'],
            ['pergunta_id' => 20, 'certa' => 1, 'texto' => 'água'],
            ['pergunta_id' => 20, 'certa' => 0, 'texto' => 'luz'],

            ['pergunta_id' => 21, 'certa' => 0, 'texto' => '2022'],
            ['pergunta_id' => 21, 'certa' => 1, 'texto' => '2030'],
            ['pergunta_id' => 21, 'certa' => 0, 'texto' => '2038'],
            ['pergunta_id' => 21, 'certa' => 0, 'texto' => '2040'],
            ['pergunta_id' => 22, 'certa' => 0, 'texto' => 'água subterrânea'],
            ['pergunta_id' => 22, 'certa' => 0, 'texto' => 'água superficial'],
            ['pergunta_id' => 22, 'certa' => 0, 'texto' => 'fiorde'],
            ['pergunta_id' => 22, 'certa' => 1, 'texto' => 'glaciar'],
            ['pergunta_id' => 23, 'certa' => 1, 'texto' => 'a preservação dos rios e das nascentes'],
            ['pergunta_id' => 23, 'certa' => 0, 'texto' => 'manutenção das construções civis'],
            ['pergunta_id' => 23, 'certa' => 0, 'texto' => 'facilitar o tratamento de esgoto'],
            ['pergunta_id' => 23, 'certa' => 0, 'texto' => 'todas as alternativas estão corretas'],
            ['pergunta_id' => 24, 'certa' => 1, 'texto' => 'prejudicam o tratamento do esgoto'],
            ['pergunta_id' => 24, 'certa' => 0, 'texto' => 'interferem nas sustentações das obras'],
            ['pergunta_id' => 24, 'certa' => 0, 'texto' => 'facilitam o tratamento de esgoto'],
            ['pergunta_id' => 24, 'certa' => 0, 'texto' => 'nenhuma alternativa está correta'],
            ['pergunta_id' => 25, 'certa' => 0, 'texto' => 'insípida, azulada, inodora'],
            ['pergunta_id' => 25, 'certa' => 0, 'texto' => 'transparente, salina, turva'],
            ['pergunta_id' => 25, 'certa' => 1, 'texto' => 'sem cor, sem cheiro, sem sabor'],
            ['pergunta_id' => 25, 'certa' => 0, 'texto' => 'inodora, incolor, turva'],
            ['pergunta_id' => 26, 'certa' => 0, 'texto' => 'catapora, hepatite, gastrite'],
            ['pergunta_id' => 26, 'certa' => 1, 'texto' => 'cólera, amebíase, hepatite'],
            ['pergunta_id' => 26, 'certa' => 0, 'texto' => 'hepatite, cólera, tétano'],
            ['pergunta_id' => 26, 'certa' => 0, 'texto' => 'tuberculose, AIDS, caxumba'],
            ['pergunta_id' => 27, 'certa' => 0, 'texto' => 'as construções'],
            ['pergunta_id' => 27, 'certa' => 1, 'texto' => 'o solo das margens dos mananciais'],
            ['pergunta_id' => 27, 'certa' => 0, 'texto' => 'invasões nas represas'],
            ['pergunta_id' => 27, 'certa' => 0, 'texto' => 'todas as alternativas estão incorretas'],
            ['pergunta_id' => 28, 'certa' => 0, 'texto' => 'queimar'],
            ['pergunta_id' => 28, 'certa' => 0, 'texto' => 'jogá-lo próximo de rios'],
            ['pergunta_id' => 28, 'certa' => 0, 'texto' => 'jogá-los todos em um único saco de lixo'],
            ['pergunta_id' => 28, 'certa' => 1, 'texto' => 'reciclar e dar a destinação correta para cada resíduo'],
            ['pergunta_id' => 29, 'certa' => 0, 'texto' => '1 minuto'],
            ['pergunta_id' => 29, 'certa' => 1, 'texto' => '5 minutos'],
            ['pergunta_id' => 29, 'certa' => 0, 'texto' => '10 minutos'],
            ['pergunta_id' => 29, 'certa' => 0, 'texto' => '15 minutos'],
            ['pergunta_id' => 30, 'certa' => 0, 'texto' => '20%'],
            ['pergunta_id' => 30, 'certa' => 0, 'texto' => '30%'],
            ['pergunta_id' => 30, 'certa' => 1, 'texto' => '40%'],
            ['pergunta_id' => 30, 'certa' => 0, 'texto' => '50%'],

            ['pergunta_id' => 31, 'certa' => 0, 'texto' => 'Brasília'],
            ['pergunta_id' => 31, 'certa' => 0, 'texto' => 'Rondônia'],
            ['pergunta_id' => 31, 'certa' => 0, 'texto' => 'São Paulo'],
            ['pergunta_id' => 31, 'certa' => 1, 'texto' => 'Rio de Janeiro'],
            ['pergunta_id' => 32, 'certa' => 0, 'texto' => 'Rio Nilo, Egito'],
            ['pergunta_id' => 32, 'certa' => 0, 'texto' => 'Rio Indo, Paquistão'],
            ['pergunta_id' => 32, 'certa' => 0, 'texto' => 'Rio Amarelo, China'],
            ['pergunta_id' => 32, 'certa' => 1, 'texto' => 'Rio Amazonas, Brasil'],
            ['pergunta_id' => 33, 'certa' => 0, 'texto' => 'profundidade'],
            ['pergunta_id' => 33, 'certa' => 0, 'texto' => 'aquosidade'],
            ['pergunta_id' => 33, 'certa' => 1, 'texto' => 'permeabilidade'],
            ['pergunta_id' => 33, 'certa' => 0, 'texto' => 'sustentabilidade'],
            ['pergunta_id' => 34, 'certa' => 0, 'texto' => 'arar a terra'],
            ['pergunta_id' => 34, 'certa' => 0, 'texto' => 'colocar água'],
            ['pergunta_id' => 34, 'certa' => 1, 'texto' => 'retirar água'],
            ['pergunta_id' => 34, 'certa' => 0, 'texto' => 'nenhuma das alternativas anteriores'],
            ['pergunta_id' => 35, 'certa' => 1, 'texto' => 'vegetação que acompanha as margens dos cursos de água'],
            ['pergunta_id' => 35, 'certa' => 0, 'texto' => 'árvores que ocupam as áreas de manguezal'],
            ['pergunta_id' => 35, 'certa' => 0, 'texto' => 'árvores que margeiam as florestas'],
            ['pergunta_id' => 35, 'certa' => 0, 'texto' => 'todas as alternativas anteriores'],
            ['pergunta_id' => 36, 'certa' => 0, 'texto' => 'proliferação de algas'],
            ['pergunta_id' => 36, 'certa' => 0, 'texto' => 'o transporte aquático'],
            ['pergunta_id' => 36, 'certa' => 0, 'texto' => 'a reprodução de peixes'],
            ['pergunta_id' => 36, 'certa' => 1, 'texto' => 'a erosão e o assoreamento'],
            ['pergunta_id' => 37, 'certa' => 0, 'texto' => 'drenadores'],
            ['pergunta_id' => 37, 'certa' => 1, 'texto' => 'decantadores'],
            ['pergunta_id' => 37, 'certa' => 0, 'texto' => 'compressores'],
            ['pergunta_id' => 37, 'certa' => 0, 'texto' => 'digestores'],
            ['pergunta_id' => 38, 'certa' => 0, 'texto' => 'manguezais'],
            ['pergunta_id' => 38, 'certa' => 0, 'texto' => 'mares'],
            ['pergunta_id' => 38, 'certa' => 1, 'texto' => 'mananciais'],
            ['pergunta_id' => 38, 'certa' => 0, 'texto' => 'todas as alternativas anteriores'],
            ['pergunta_id' => 39, 'certa' => 0, 'texto' => 'limnologia'],
            ['pergunta_id' => 39, 'certa' => 1, 'texto' => 'hidrologia'],
            ['pergunta_id' => 39, 'certa' => 0, 'texto' => 'hidrografia'],
            ['pergunta_id' => 39, 'certa' => 0, 'texto' => 'oceanografia'],
            ['pergunta_id' => 40, 'certa' => 0, 'texto' => 'optando pela compra de produtos com embalagens recicláveis'],
            ['pergunta_id' => 40, 'certa' => 0, 'texto' => 'reutilizando os materiais e objetos sempre que possível'],
            ['pergunta_id' => 40, 'certa' => 0, 'texto' => 'apoiando iniciativas de reciclagem'],
            ['pergunta_id' => 40, 'certa' => 1, 'texto' => 'todas as alternativas estão corretas'],

            ['pergunta_id' => 41, 'certa' => 1, 'texto' => 'infiltração, escoamento, reciclagem de nutrientes, precipitação'],
            ['pergunta_id' => 41, 'certa' => 0, 'texto' => 'infiltração, sistema de abastecimento de água e saneamento, escoamento'],
            ['pergunta_id' => 41, 'certa' => 0, 'texto' => 'escoamento, sistema de drenagem, sistema de abastecimento de água e saneamento, reciclagem de nutrientes'],
            ['pergunta_id' => 41, 'certa' => 0, 'texto' => 'infiltração, escoamento, precipitação e sistema de regas'],
            ['pergunta_id' => 42, 'certa' => 0, 'texto' => 'população, pois a mesma poderá consumir a água em abundância'],
            ['pergunta_id' => 42, 'certa' => 1, 'texto' => 'gestão de recursos hídricos'],
            ['pergunta_id' => 42, 'certa' => 0, 'texto' => 'empresa, pois poderá cobrar mais barato pelos seus produtos'],
            ['pergunta_id' => 42, 'certa' => 0, 'texto' => 'todas as alternativas estão corretas.'],
            ['pergunta_id' => 43, 'certa' => 0, 'texto' => 'construindo uma casa na árvore'],
            ['pergunta_id' => 43, 'certa' => 1, 'texto' => 'reciclando papéis, jornais e revistas'],
            ['pergunta_id' => 43, 'certa' => 0, 'texto' => 'reutilizando metais e vidros'],
            ['pergunta_id' => 43, 'certa' => 0, 'texto' => 'indo a parques'],
            ['pergunta_id' => 44, 'certa' => 0, 'texto' => 'água corrente'],
            ['pergunta_id' => 44, 'certa' => 0, 'texto' => 'petróleo'],
            ['pergunta_id' => 44, 'certa' => 1, 'texto' => 'barra de ferro'],
            ['pergunta_id' => 44, 'certa' => 0, 'texto' => 'sol'],
            ['pergunta_id' => 45, 'certa' => 0, 'texto' => '0° C'],
            ['pergunta_id' => 45, 'certa' => 0, 'texto' => '36° C'],
            ['pergunta_id' => 45, 'certa' => 0, 'texto' => '50° C'],
            ['pergunta_id' => 45, 'certa' => 1, 'texto' => '100° C'],
            ['pergunta_id' => 46, 'certa' => 0, 'texto' => 'em média 3 dias'],
            ['pergunta_id' => 46, 'certa' => 1, 'texto' => 'em média 5 dias'],
            ['pergunta_id' => 46, 'certa' => 0, 'texto' => 'em média 15 dias'],
            ['pergunta_id' => 46, 'certa' => 0, 'texto' => 'em média 30 dias'],
            ['pergunta_id' => 47, 'certa' => 0, 'texto' => 'em média 20%'],
            ['pergunta_id' => 47, 'certa' => 0, 'texto' => 'em média 50%'],
            ['pergunta_id' => 47, 'certa' => 1, 'texto' => 'em média 70%'],
            ['pergunta_id' => 47, 'certa' => 0, 'texto' => 'em média 90%'],
            ['pergunta_id' => 48, 'certa' => 0, 'texto' => 'SOH'],
            ['pergunta_id' => 48, 'certa' => 1, 'texto' => 'H2O'],
            ['pergunta_id' => 48, 'certa' => 0, 'texto' => 'HO3'],
            ['pergunta_id' => 48, 'certa' => 0, 'texto' => 'H2OH'],
            ['pergunta_id' => 49, 'certa' => 0, 'texto' => 'cerca de 95%;'],
            ['pergunta_id' => 49, 'certa' => 1, 'texto' => 'cerca de 75%;'],
            ['pergunta_id' => 49, 'certa' => 0, 'texto' => 'cerca de 55%;'],
            ['pergunta_id' => 49, 'certa' => 0, 'texto' => 'cerca de 25%.'],
            ['pergunta_id' => 50, 'certa' => 0, 'texto' => 'são importantes corredores ecológicos que favorecem a conservação da biodiversidade'],
            ['pergunta_id' => 50, 'certa' => 0, 'texto' => "contribuem para o controle e redução do processo de assoreamento dos cursos d'água"],
            ['pergunta_id' => 50, 'certa' => 1, 'texto' => 'intensificam a força das águas que chegam aos rios nos períodos de precipitação acentuada'],
            ['pergunta_id' => 50, 'certa' => 0, 'texto' => 'oferecem proteção para as águas e o solo, evitando a erosão e o desbarrancamento nas margens dos rios, córregos e lagos'],

        ];
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seed estático
        // $perguntas = Pergunta::all();
        // foreach($perguntas as $pergunta)
        // {
        //     if(count($pergunta->alternativas) == 0)
        //     {
        //         $alternativaCorreta = rand(0, 4);
        //         for($i = 0; $i < 4; $i++)
        //         {
        //             $certa = $i == $alternativaCorreta? true: false;
        //             $texto = $certa? 'correta': 'errada';
        //             Alternativa::create(['pergunta_id' => $pergunta->id,
        //                                 'texto' => $texto,
        //                                 'certa' => $certa]);
        //         }
        //     }
        // }
        foreach($this->alternativas() as $alternativa)
        {
            Alternativa::create($alternativa);
        }
    }
}
