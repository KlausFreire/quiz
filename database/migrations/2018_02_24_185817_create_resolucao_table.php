<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateResolucaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resolucao', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('usuario_id')->unsigned();
            $table->integer('questionario_id')->unsigned();
            $table->boolean('finalizado')->default(false);
            $table->dateTime('data_inicio');
            $table->integer('tempo')->nullable();
            $table->integer('total_acertos')->default(0);
            
            $table->foreign('usuario_id')
                ->references('id')->on('usuario');
            $table->foreign('questionario_id')
                ->references('id')->on('questionario');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('resolucao');
        Schema::enableForeignKeyConstraints();
    }
}
