<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespostaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resposta', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('resolucao_id')->unsigned();
            $table->integer('alternativa_id')->unsigned();

            $table->foreign('resolucao_id')
                ->references('id')->on('resolucao');
            $table->foreign('alternativa_id')
                ->references('id')->on('alternativa');
            $table->unique(['resolucao_id', 'alternativa_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('resposta');
        Schema::enableForeignKeyConstraints();
    }
}
